import React, { Component } from 'react';
import {
    Card,
    CardHeader,
    CardTitle,
    CardImg,
    CardBody,
    CardFooter,
    Button
} from "shards-react";

import "shards-ui/dist/css/shards.min.css";
import "./styles/cards.css"

class GpCard extends Component {
    render() {
        const id = Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15);
        return <Card className={`gearPlugCard ${ this.props.color ? this.props.color : 'default' }`}>

        <CardHeader>{ this.props.headerTitle ? this.props.headerTitle : 'Default' } { this.props.icon ? <i className="fa fas fa-${ this.props.icon }"></i> : <i className="fas fa-chevron-up"></i> } <button data-toggle="collapse" data-target={`#${ this.props.name ? this.props.name : id }`}>X</button> </CardHeader>
        { this.props.image ?  <CardImg src={ this.props.image ? this.props.image : 'https://place-hold.it/300x200' }/> : null }
        { this.props.title || this.props.content ? <CardBody className="collapse in" id={`${ this.props.name ? this.props.name : id }`}>
        <CardTitle>{ this.props.title }</CardTitle>
        <p>{ this.props.content }</p>
        { this.props.buttonText ? <Button>{ this.props.buttonText }</Button> : null }
        </CardBody> : null }
        { this.props.footer ? <CardFooter>{ this.props.footer }</CardFooter> : null }
        </Card>
    }
}

export default GpCard;