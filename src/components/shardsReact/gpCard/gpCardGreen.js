import React, { Component } from 'react';
import GpCard from './gpCard';

class GpCardGreen extends Component {
    render () {
        return <GpCard {...this.props} color="green-card"/>
    }
}

export default GpCardGreen;