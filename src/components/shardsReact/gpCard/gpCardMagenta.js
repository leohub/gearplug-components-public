import React, { Component } from 'react';
import GpCard from './gpCard';

class GpCardMagenta extends Component {
    render () {
        return <GpCard {...this.props} color="magenta-card"/>
    }
}

export default GpCardMagenta;