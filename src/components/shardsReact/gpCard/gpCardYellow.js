import React, { Component } from 'react';
import GpCard from './gpCard';

class GpCardYellow extends Component {
    render () {
        return <GpCard {...this.props} color="yellow-card"/>
    }
}

export default GpCardYellow;