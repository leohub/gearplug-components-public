import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Template from './pages/template.js';


class Root extends Component {
    render() {
        return (
            <Template/>
            )
        }
    }
    
    let container = document.getElementById('app');
    let component = <Root />;
    ReactDOM.render(component, container);

    module.hot.accept();