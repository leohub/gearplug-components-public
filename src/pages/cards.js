import React, { Component } from "react";
import GpCard from "../components/shardsReact/gpCard/gpCard"
import GpCardGreen from "../components/shardsReact/gpCard/gpCardGreen"
import GpCardYellow from "../components/shardsReact/gpCard/gpCardYellow"
import GpCardMagenta from "../components/shardsReact/gpCard/gpCardMagenta";
 
class Cards extends Component {
  render() {
    return (
      <div>
        <h2>Cards</h2>
        <GpCard headerTitle='¿Qué estoy haciendo ahora?' content='Estás creando la conexión con la app de destino, que recibirá los datos.' />

        <GpCardGreen icon="question-circle" headerTitle="¿Qué estoy haciendo ahora?" content="Estás creando la conexión con la app de destino, que recibirá los datos."/>

        <GpCardYellow icon="exclamation-circle" headerTitle="Cómo debo continuar" content="Presiona Conectar para establecer una conexión entre GearPlug y tu app. Debes tener los datos de acceso."/>

        <GpCardMagenta icon="exclamation-triangle" headerTitle="Antes de seguir" content="Antes de presionar el botón Probar debes ir al formulario de Facebook Leads y llenar un registro de prueba, con el fin de verificar la conexión."/>
      </div>
    );
  }
}

export default Cards;