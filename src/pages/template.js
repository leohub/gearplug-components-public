import React, { Component } from 'react';
import {
    Route,
    NavLink,
    HashRouter
} from "react-router-dom";
import Home from "./home";
import Cards from "./cards";
import Icons from "./icons";

class Template extends Component {
    render() {
        return (
        <HashRouter>
            <div id="wrapper">
                <div className="navbar navbar-inverse navbar-fixed-top">
                    <div className="adjust-nav">
                        <div className="navbar-header">
                            <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                                <span className="icon-bar"></span>
                            </button>
                            <a className="navbar-brand" href="#">
                                <img src="assets/img/logo.png" />
                            </a>
                        </div>
                        
                        <span className="logout-spn" >
                            <a href="#">LOGOUT</a>  
                            
                        </span>
                    </div>
                </div>
                
                <nav className="navbar-default navbar-side" role="navigation">
                    <div className="sidebar-collapse">
                        <ul className="nav" id="main-menu">
                            <li><NavLink to="/">Home</NavLink></li>
                            <li><NavLink to="/cards">Cards</NavLink></li>
                            <li><NavLink to="/icons">Icons</NavLink></li>
                        </ul>
                    </div>
                </nav>
            </div>
            <div id="page-wrapper" >
                <div id="page-inner">
                    <Route exact path="/" component={Home}/>
                    <Route path="/cards" component={Cards}/>
                    <Route path="/icons" component={Icons}/>
                </div>
            </div>

        </HashRouter>
        )
    }
}

export default Template;
